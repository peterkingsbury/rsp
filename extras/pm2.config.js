module.exports = {
  apps : [{
    name        : 'rsp-server',
    script      : './node_modules/.bin/actionhero',
    watch       : false,
    env: {
      "NODE_ENV": "development"
    },
    cwd         : '/var/www/rsp-server',
    args        : 'start',
    env_production : {
      "NODE_ENV": "production"
    }
  }, {
    name        : 'rsp-ui',
    script      : './node_modules/.bin/webpack-dev-server',
    watch       : false,
    env: {
      "NODE_ENV": "development"
    },
    cwd         : '/var/www/rsp-server',
    args        : '--content-base public/ --config webpack.dev.config.js --inline --host 0.0.0.0 --port 8080',
    env_production : {
      "NODE_ENV": "production"
    }
  }]
};
