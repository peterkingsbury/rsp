#!/bin/bash

# Install nginx
apt-get update
apt-get install -y nginx

# Install nvm
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.2/install.sh | bash
bash
nvm install v6

# Install MariaDB
apt-get install -y mariadb-server mariadb-client

# Install redis
apt-get install -y redis-server

# Install PM2
npm install -g pm2
