//require('file?name=[name].[ext]!../index.html');
import '../css/app.scss';
import $ from "jquery";
import '../../node_modules/bootstrap/dist/js/bootstrap.min.js';
import 'waypoints';
import 'scrollTo';
//import * as ActionheroClient from './ws';

$(function() {
  let client = new ActionheroClient;

  client.connect(function(error, details){
    if(error != null){
      console.log(error);
    }

    client.action('status', {}, function(data) {
      console.log(data);
    });
  });

  client.on('connected',    function(){ console.log('connected!') });
  client.on('disconnected', function(){ console.log('disconnected :(') });

  client.on('error',        function(error){ console.log('error', error.stack) });
  client.on('reconnect',    function(){ console.log('reconnect') });
  client.on('reconnecting', function(){ console.log('reconnecting') });

  // client.on('message',      function(message){ console.log(message) })

  client.on('alert',        function(message){ alert( JSON.stringify(message) ) });
  client.on('api',          function(message){ alert( JSON.stringify(message) ) });

  client.on('welcome',      function(message){ console.log(message); });
  client.on('say',          function(message){ console.log(message); });
});

/*
import '../css/app.scss';
import $ from "jquery";
import '../../node_modules/bootstrap/dist/js/bootstrap.min.js';
import 'waypoints';
import 'scrollTo';

// * EXAMPLE JS STARTS
$(function() {
    $('[id^=scrollTo]').click(function() {
        var id = $(this).attr('id').slice(9);
        $(window).scrollTo($('#' + id), 1000, { offset: { top: -51, left: 0 } });
    });

    $('#marketing').waypoint(function() {
        $('.img-circle').addClass('animated zoomIn');
    }, {
        offset: '50%',
        triggerOnce: true
    });

    $('.featurette').waypoint(function() {
        $('#' + this.element.id + ' .featurette-image').addClass('animated pulse');
    }, {
        offset: '50%',
        triggerOnce: true
    });
});

// * EXAMPLE JS ENDS
*/
