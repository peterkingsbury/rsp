# Real-Time Socket Platform

## To Install
`npm install`

## Starting the Server

### Single Node
`npm start`

### Cluster Mode
`npm run cluster`

## Starting the User Interface

### Building for Deployment
`npm run build`

### Running the Dev Environment
`npm run dev`

## Testing the Server

### Tests Only
`npm test`

### Test Coverage
`npm run coverage`

